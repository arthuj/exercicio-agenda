package br.com.itau;

import java.sql.ClientInfoStatus;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Agenda {

    private int numeroContatos;

    public Agenda() {
    }

    public int getNumeroContatos() {
        return numeroContatos;
    }

    public void setNumeroContatos(int numeroContatos) {
        this.numeroContatos = numeroContatos;
    }

    public ArrayList<Contato> CriarListaContatos(int qtdContatos)
    {
        ArrayList<Contato> lista= new ArrayList<Contato>();
        Scanner scanner = new Scanner(System.in);
        for(int i = 1; i <= qtdContatos; i++)
        {
            Contato contato = new Contato();
            System.out.println("Qual o nome do contato ?");
            contato.setNome(scanner.next());
            System.out.println("Qual o número de telefone ?");
            contato.setNumero(scanner.nextInt());
            System.out.println("Qual o email ?");
            contato.setEmail(scanner.next());
            lista.add(contato);
            System.out.println("Usuário Adicionado");
        }
        return  lista;
    }
    public void ExibirAgenda(List<Contato> listaContato)
    {
        for (Contato contato: listaContato)
        {
            System.out.println("Nome: " + contato.getNome() + " Email: " + contato.getEmail() + " Número: " + contato.getNumero());
        }
    }

    public  void ExibirContato(int numero, ArrayList<Contato> lista)
    {
        for (Contato contato: lista)
        {
            if(contato.getNumero() == numero)
                System.out.println(contato);
        }
    }

    public  void ExibirContato(String email, ArrayList<Contato> lista)
    {
        for (Contato contato: lista)
        {
            if(contato.getEmail() == email)
                System.out.println(contato);
        }
    }

    public void RemoverContato(int numero, ArrayList<Contato> lista)
    {
        lista.removeIf(l -> l.getNumero() == numero);
    }
    public  void RemoverContato(String email,ArrayList<Contato> lista)
    {
        lista.removeIf(l -> l.getEmail().equals(email));
    }
}
