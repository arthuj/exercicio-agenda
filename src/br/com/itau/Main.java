package br.com.itau;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Agenda agenda = new Agenda();
        ArrayList<Contato> listaContatos = new ArrayList<Contato>();
        System.out.println("Olá, qual o número de contatos da agenda ?");
        agenda.setNumeroContatos(scanner.nextInt());
        listaContatos = agenda.CriarListaContatos(agenda.getNumeroContatos());
        agenda.ExibirAgenda(listaContatos);

    }
}
